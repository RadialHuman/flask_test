from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return "Something"

@app.route('/something')
def secondPage():
    return "Something else"

if __name__ == "__main__":
    app.run(debug=True)